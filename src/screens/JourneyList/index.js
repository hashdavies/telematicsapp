import {Alert, FlatList, StyleSheet, Text, View} from 'react-native';
import React, {useEffect} from 'react';
import {useNavigation} from '@react-navigation/native';
import Back4AppUtility from '../../utils/Back4AppUtility';
import ButtonComp from '../../Components/Ui/ButtonComp';
import {ScrollView} from 'react-native-gesture-handler';
import JourneyListItems from './List';
import LoaderScreen from '../../Components/LoaderScreen';
import {fontFamilySelector} from '../../utils/fonts';
import {SvgXml} from 'react-native-svg';
import {CarIcon, NotificationIcon, SunIcon} from '../../utils/svg';

const JourneyList = ({navigation}) => {
  const {setOptions, toggleDrawer} = useNavigation();
  const [loading, setLoading] = React.useState(false);
  const [allAvailableJourney, setAllAvailableJourney] = React.useState([]);

  useEffect(() => {
    getUserPastJourney();
  }, []);
  const getMetricsForJourney = async journeyId => {
    try {
      const queryConditions = [
        {
          journeyId: journeyId,
        },
      ];
      const metrics = await Back4AppUtility.queryRecords(
        'Metrics',
        queryConditions,
      );
      return metrics;
    } catch (error) {
      console.log(error, 'metric error');
      // throw error;
    }
  };

  const getUserPastJourney = async () => {
    let TableClass = 'Journey';
    try {
      setLoading(true);
      const schoolClasses = await Back4AppUtility.getAllRecord(TableClass);
      // const schoolClasses = await Back4AppUtility.getAllRecord2(TableClass);

      let res = schoolClasses?.results;
      // console.log('<<<<<<<<<results');
      console.log(res, 'results');
      // console.log('results>>>>>>>>');
      for (const journey of res) {
        console.log(journey, 'each joureny');
        const journeyId = journey.objectId;
        const metrics = await getMetricsForJourney(journeyId);

        // Now you have the journey and its associated metrics
        console.log('Journey:', journey);
        console.log('Metrics:', metrics);
        journey.metricsData = metrics;
      }

      setAllAvailableJourney(res?.reverse());
      setLoading(false);
    } catch (error) {
      console.log(error, 'error.response.data');
      setAllAvailableJourney([]);
      console.error('Error:', error);
      setLoading(false);
      Alert.alert('An error occurred. Please try again.');
    }
  };
  const startNewJourney = () => {
    navigation.navigate('newJourney');
  };
  return (
    <View style={{flex: 1}}>
      <View
        style={{
          backgroundColor: '#0148AB',
          minHeight: 100,
          paddingHorizontal: 24,
          paddingTop: 32,
          paddingBottom: 7,
        }}>
        <View>
          <View
            style={{
              flexDirection: 'row',
              gap: 10,
              alignItems: 'center',
              justifyContent: 'space-between',
              marginBottom: 21,
            }}>
            <View
              style={{
                flexDirection: 'row',
                gap: 10,
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <View
                style={{
                  height: 34,
                  width: 34,
                  backgroundColor: '#fff',
                  borderRadius: 100,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    color: '#0148AB',
                    fontSize: 16,
                    fontFamily: fontFamilySelector?.fontGilory,
                  }}>
                  J
                </Text>
              </View>
              <View>
                <View
                  style={{
                    flexDirection: 'row',
                    gap: 10,
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      color: '#fff',
                      fontSize: 12,
                      fontFamily: fontFamilySelector?.fontGilory,

                      // fontWeight:9
                    }}>
                    Good Afternoon
                  </Text>
                  <SvgXml xml={SunIcon} />
                </View>

                <Text
                  style={{
                    color: '#fff',
                    fontSize: 14,
                    fontFamily: fontFamilySelector?.fontGilory,
                    // fontWeight:9
                  }}>
                  Jones Alausa
                </Text>
              </View>
            </View>
            <View>
              <SvgXml xml={NotificationIcon} />
            </View>
          </View>
          <View
            style={{
              backgroundColor: '#fff',
              paddingTop: 7,
              paddingBottom: 6,
              borderRadius: 10,
              flexDirection: 'row',
              gap: 12,
            }}>
            <View
              style={{
                backgroundColor: '#0148AB',
                // paddingLeft:10,
                width: 5,
                height: '100%',
                marginLeft: 1,
                borderTopRightRadius: 2,
                borderTopLeftRadius: 2,
                borderBottomRightRadius: 2,
                borderBottomLeftRadius: 2,
              }}
            />
            <View
              style={{
                paddingVertical: 8,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  gap: 10,
                  marginBottom: 5,
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: fontFamilySelector?.fontGilory,
                    fontSize: 12,
                    color: '#0148AB',
                  }}>
                  23, Joel Ogunnaike street, Ikeja GRA
                </Text>
                <SvgXml xml={CarIcon} />
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  gap: 5,
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 4,
                    height: 4,
                    borderRadius: 100,
                    backgroundColor: '#19941E',
                  }}
                />
                <Text
                  style={{
                    fontFamily: fontFamilySelector?.fontGilory,
                    fontSize: 10,
                    color: '#19941E',
                  }}>
                  User Journey Data
                </Text>
              </View>
            </View>
          </View>
        </View>
      </View>
      <View
        style={{
          paddingHorizontal: 24,
          paddingVertical: 12,
          borderWidth: 0.3,
          borderColor: '#0148AB',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}>
        <Text
          style={{
            fontFamily: fontFamilySelector?.fontGilory,
            color: '#0148AB',
            fontSize: 12,
            fontWeight: 600,
          }}>
          Journey History
        </Text>
        <ButtonComp
          btnText={'Start New Trip'}
          btnTextStyle={{
            fontSize: 12,
          }}
          containerStyle={{
            backgroundColor: '#0148AB',
            paddingHorizontal: 10,
            paddingVertical: 10,
            borderRadius: 5,
            height: 40,
            marginTop: 5,
          }}
          onPress={startNewJourney}
        />
      </View>
      {/* <ButtonComp
        btnText={'Start New Journey'}
        btnTextStyle={styles?.btnText}
        containerStyle={{
          backgroundColor: '#25a6f0',
          paddingHorizontal: 10,
          borderRadius: 20,
          height: 40,
          marginTop: 5,
        }}
        onPress={startNewJourney}
      /> */}
      <ScrollView
        nestedScrollEnabled
        scrollEnabled={true}
        style={{flex: 1}}
        contentContainerStyle={{}}>
        <View style={{flex: 1, paddingBottom: 150}}>
          <FlatList
            nestedScrollEnabled
            data={allAvailableJourney}
            horizontal={false}
            automaticallyAdjustContentInsets
            bounces={false}
            keyboardShouldPersistTaps="never"
            showsHorizontalScrollIndicator={false}
            renderItem={({item, index}) => (
              <JourneyListItems
                dataObject={item}
                index={index}
                navigation={navigation}
              />
            )}
          />

          <LoaderScreen isVisible={loading} />
        </View>
      </ScrollView>
    </View>
  );
};

export default JourneyList;

const styles = StyleSheet.create({});

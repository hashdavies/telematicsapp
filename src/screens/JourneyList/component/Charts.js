import React from 'react';
import {View, Text, StyleSheet, Dimensions} from 'react-native';
import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart,
} from 'react-native-chart-kit';
const screenWidth = Dimensions.get('window').width;
const chartConfig = {
  backgroundGradientFrom: 'red',
  backgroundGradientFromOpacity: 0,
  backgroundGradientTo: 'purple',
  backgroundGradientToOpacity: 0.5,
  color: (opacity = 1) => `#000`,
  strokeWidth: 2, // optional, default 3
  barPercentage: 0.5,
  useShadowColorFromDataset: false, // optional
};

const Charts = ({value, legend}) => {
  console.log(value, 'value');
  const data = {
    // labels: ['January', 'February', 'March', 'April', 'May', 'June'],
    labels: [],
    datasets: [
      {
        // data: [
        //   20, 45, 28, 80, 99, 43, 20, 45, 28, 80, 99, 43, 20, 45, 28, 80, 99,
        //   43, 300, 500,
        // ],
        data: value,
        color: (opacity = 1) => `rgba(134, 65, 244, ${opacity})`, // optional
        strokeWidth: 2, // optional
      },
    ],
    // legend: [legend], // optional
  };
  return (
    <View style={styles.container}>
      <View>
        <LineChart
          data={data}
          width={screenWidth}
          height={220}
          chartConfig={chartConfig}
        />
      </View>
      <Text style={{color: 'red', textAlign: 'center'}}>{legend}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 20,
    color: '#000',
  },
});

export default Charts;

import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Charts from './Charts';
const VehicleDashboard = ({vehicleData}) => {
  console.log(vehicleData, 'vehicleData');
  return (
    <View style={styles.container}>
      <Charts value={vehicleData?.speedData} legend="Speedometer" />
      <Charts value={vehicleData?.rpmData} legend="RPM Guage" />
      <Charts value={vehicleData?.fuelLevelData} legend="Fuel Level" />
      <Charts value={vehicleData?.engineTemperature} legend="Temperature" />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
    justifyContent: 'center',
    alignItems: 'center',
  },
  dataCard: {
    width: 150,
    height: 100,
    backgroundColor: '#fff',
    borderRadius: 8,
    padding: 10,
    margin: 10,
    elevation: 3,
    alignItems: 'center',
    justifyContent: 'center',
  },
  label: {
    fontSize: 14,
    fontWeight: 'bold',
    marginBottom: 5,
    color: '#000',
  },
  value: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#000',
  },
});

export default VehicleDashboard;

import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {scale} from 'react-native-size-matters';
import { fontFamilySelector } from '../../utils/fonts';
import moment from 'moment';

export default function JourneyListItems({dataObject, index, navigation}) {
  console.log(dataObject, 'dataObject');
  const {originName, destination, createdAt, metricsData} = dataObject;
  console.log(originName, 'originName');
  const gotoJourneyDetails = dataObject => {
    navigation.navigate('journeyDetails', {dataObject});
  };
  return (
    <TouchableOpacity onPress={() => gotoJourneyDetails(dataObject)}>
      <View style={styles.listItem} key={index}>
        <View style={styles.leftColumn}>
        <View
        style={{
          flexDirection:'row',
          justifyContent:'space-between'
        }}
        >
        <Text
            style={
              styles.locationText
            }>{`${originName} - ${destination}`}</Text>
             <Text
            style={
              styles.locationText
            }>80km/hr</Text>
        </View>
          <Text style={[styles.timeText,{fontWeight:'300'}]}>{` ${moment(createdAt).calendar()}`}</Text>
        </View>
        <View style={styles.rightColumn}>
          {/* <Text style={styles.speedText}>{speed}</Text> */}
        </View>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  listItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 16,
  },
  leftColumn: {
    flex: 1,
  },
  rightColumn: {
    marginLeft: 16,
  },
  locationText: {
    fontSize: 12,
    // fontWeight: 'bold',
    color: '#2D2D2D',
    fontFamily:fontFamilySelector?.fontGilory
  },
  timeText: {
    fontSize: 12,
    color: '#808080',
    marginTop: 10,
  },
  speedText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#2D2D2D',
  },
});

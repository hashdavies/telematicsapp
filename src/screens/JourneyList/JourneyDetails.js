import {Button, ImageBackground, ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React, {useEffect, useState} from 'react';
import {useNavigation} from '@react-navigation/native';
import {scale} from 'react-native-size-matters';
import {generateSimulatedData} from '../../utils/DataGenerator';
import Back4AppUtility from '../../utils/Back4AppUtility';
import VehicleDashboard from './component/vehicleDashboard';
import { car } from '../../assets/image';
import { SvgXml } from 'react-native-svg';
import { BackArrow, FuelLevel, RPMGuage, SpeedometerIcon, TemperatureIcon } from '../../utils/svg';
import { fontFamilySelector } from '../../utils/fonts';

export default function JourneyDetails({route}) {
  const {dataObject} = route.params;
  const navigation =useNavigation()
  useEffect(() => {
    navigation.setOptions({
      title: '',
      headerStyle: {
        backgroundColor: 'white',
      },
      headerTintColor: '#0148AB',
      headerLeft: () => (
        <TouchableOpacity onPress={()=>navigation.goBack()} style={{paddingLeft:20}}>
          <SvgXml xml={BackArrow}/>
        </TouchableOpacity>
      ),
      headerRight: () => {},
      headerTitleAlign: 'center', // Center-align the header title
      headerTitle: () => (
        <View style={{ paddingHorizontal: 16 }}>
          <Text style={{color:'#0148AB',fontFamily:fontFamilySelector?.fontGilory,fontWeight:500,fontSize:scale(16)
          }
          }>Journey History</Text>
        </View>
      ),

    });
  }, []);
  const [vehicleData, setVehicleData] = useState(generateSimulatedData());
  // const [journeyId, setJourneyId] = useState(null);
  const [journeySpeedData, setJourneySpeedData] = useState([]);
  const [formattedJourneyData, setFormattedJourneyData] = useState({});

  // const recordMetric = metric => {
  //   // console.log(metric, 'Current Metric');
  //   if (isJourneyInProgress) {
  //     console.log(metric, 'Current Metric');
  //     // Accumulate metrics during the journey
  //     setJourneyMetrics(prevMetrics => [...prevMetrics, metric]);
  //   }
  // };

  useEffect(() => {
    if (dataObject) {
      console.log(JSON.stringify(dataObject), 'dataObject');
      let metricsData = dataObject?.metricsData?.[0]?.metrics;
      console.log(metricsData, 'metricsData');
      // const speedData = metricsData.map(metric => metric.speed);

      const speedData = getMetricData(metricsData, 'speed');
      const rpmData = getMetricData(metricsData, 'rpm');
      const fuelLevelData = getMetricData(metricsData, 'fuelLevel');
      const engineTemperature = getMetricData(metricsData, 'engineTemperature');
      console.log(speedData, 'speedData');
      console.log(rpmData, 'rpmData');
      console.log(fuelLevelData, 'fuelLevelData');
      let dataValues = {
        speedData,
        rpmData,
        fuelLevelData,
        engineTemperature,
      };
      setFormattedJourneyData(dataValues);
    }
  }, [dataObject]);
  const getMetricData = (metricsData, metricType) => {
    // Use the map function to create an array of the specified metric data
    const metricArray = metricsData.map(metric => metric[metricType]);

    return metricArray;
  };
  // useEffect(() => {
  //   let intervalId; // Declare the interval variable outside the useEffect

  //   if (isJourneyInProgress) {
  //     // Start the interval only if the journey is in progress
  //     intervalId = setInterval(() => {
  //       const newData = generateSimulatedData();
  //       setVehicleData(newData);

  //       // Record the simulated metrics during the journey
  //       if (isJourneyInProgress) {
  //         // recordMetric({type: 'speed', value: newData.speed});
  //         // recordMetric({type: 'rpm', value: newData.rpm});
  //         // recordMetric({type: 'fuelLevel', value: newData.fuelLevel});
  //         // recordMetric({
  //         //   type: 'engineTemperature',
  //         //   value: newData.engineTemperature,
  //         // });
  //         // recordMetric({type: 'latitude', value: newData.latitude});
  //         // recordMetric({type: 'longitude', value: newData.longitude});
  //         recordMetric(newData);
  //       }
  //     }, 5000);
  //   }

  //   // Clear the interval when the component unmounts or when the journey ends
  //   return () => clearInterval(intervalId);
  // }, [isJourneyInProgress]);

  // Start journey function
  const startJourney = async () => {
    let TableClass = 'Journey';
    const newData = {
      name: 'Testing',
    };
    console.log(newData, 'newData');
    const newJourney = await Back4AppUtility.createRecord(TableClass, newData);
    console.log('Record created:', newJourney);

    // Set the journeyId to the newly created object's ID
    setJourneyId(newJourney.objectId);
    setIsJourneyInProgress(true);
    // }
  };
  const stopJourney = async () => {
    if (journeyId) {
      // Create a new Metrics object and associate it with the Journey
      // const Metrics = 'Metrics';
      // const newMetrics = new Metrics();
      // newMetrics.set('journeyId', journeyId);
      // newMetrics.set('metrics', journeyMetrics);

      // // Save the Metrics object to Back4App
      // await newMetrics.save();
      let TableClass = 'Metrics';
      const newData = {
        journeyId: journeyId,
        metrics: journeyMetrics,
      };
      console.log(newData, 'newData');
      const newMetrics = await Back4AppUtility.createRecord(
        TableClass,
        newData,
      );
      console.log(newMetrics, 'newMetrics');
      // Reset the journeyId, metrics, and journey status
      setJourneyId(null);
      setJourneyMetrics([]);
      setIsJourneyInProgress(false);
    }
  };

  return (
    <ScrollView style={{flex: 1, backgroundColor: '#fff'}}>
      {formattedJourneyData && Object.keys(formattedJourneyData).length > 0 && (
        <VehicleDashboard vehicleData={formattedJourneyData} />
      )}
      {/* <View>
        <ImageBackground
        source={car}
        imageStyle={{height:'300'}}
        style={{height:300,width:'100%',marginBottom:20}}
        />
        <View
        style={{
          paddingHorizontal:24,
          flex:1,
          flexDirection:'row',
          justifyContent:'space-between',
          flexWrap:'wrap',
          marginBottom:20
        }}
        >
          <View
          style={{backgroundColor:'#eef6f6',minHeight:30,flex:0.48,paddingVertical:6,paddingHorizontal:16}}
          >
            <SvgXml xml={SpeedometerIcon} style={{marginBottom:6}}/>
            <Text
            style={{
              color:'#DF0707',
              fontSize:12,
              fontFamily:fontFamilySelector?.fontGilory,
              marginBottom:16
            }}
            >Speedometer</Text>
             <Text
            style={{
              color:'#DF0707',
              fontSize:16,
              fontFamily:fontFamilySelector?.fontGilory,
              marginBottom:16,
              fontWeight:700
            }}
            >120 km/hr</Text>
          </View>
          <View
          style={{backgroundColor:'#eef6f6',minHeight:30,flex:0.48,paddingVertical:6,paddingHorizontal:16}}
          >
            <SvgXml xml={RPMGuage} style={{marginBottom:3}}/>
            <Text
            style={{
              color:'#C07B13',
              fontSize:12,
              fontFamily:fontFamilySelector?.fontGilory,
              marginBottom:16
            }}
            >RPM Gauge</Text>
             <Text
            style={{
              color:'#C07B13',
              fontSize:16,
              fontFamily:fontFamilySelector?.fontGilory,
              marginBottom:16,
              fontWeight:700
            }}
            >1420</Text>
          </View>
          
        </View>
        <View
        style={{
          paddingHorizontal:24,
          flex:1,
          flexDirection:'row',
          justifyContent:'space-between',
          flexWrap:'wrap'
        }}
        >
          <View
          style={{backgroundColor:'#eef6f6',minHeight:30,flex:0.48,paddingVertical:6,paddingHorizontal:16}}
          >
            <SvgXml xml={FuelLevel} style={{marginBottom:3}}/>
            <Text
            style={{
              color:'#218A10',
              fontSize:12,
              fontFamily:fontFamilySelector?.fontGilory,
              marginBottom:16
            }}
            >Fuel Level</Text>
             <Text
            style={{
              color:'#218A10',
              fontSize:16,
              fontFamily:fontFamilySelector?.fontGilory,
              marginBottom:16,
              fontWeight:700
            }}
            >Low</Text>
          </View>
          <View
          style={{backgroundColor:'#eef6f6',minHeight:30,flex:0.48,paddingVertical:6,paddingHorizontal:16}}
          >
            <SvgXml xml={TemperatureIcon} style={{marginBottom:3}}/>
            <Text
            style={{
              color:'#0148AB',
              fontSize:12,
              fontFamily:fontFamilySelector?.fontGilory,
              marginBottom:16
            }}
            >Temperature</Text>
             <Text
            style={{
              color:'#0148AB',
              fontSize:16,
              fontFamily:fontFamilySelector?.fontGilory,
              marginBottom:16,
              fontWeight:700
            }}
            >120 
            o
            C</Text>
          </View>
          
        </View>
      </View> */}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  Box: {
    backgroundColor: '#d9d9d9',
    marginVertical: 10,
    marginHorizontal: 10,
    paddingHorizontal: 10,
    paddingVertical: 10,
    borderRadius: 10,
  },
  btnText: {
    fontWeight: '600',
    fontSize: 13,
  },
  titleStyle: {
    color: 'black',
    fontWeight: '600',
    fontSize: 13,
    marginBottom: scale(4),
  },
  descStyle: {
    fontSize: 10,
    color: 'black',
    fontWeight: '600',
  },
});

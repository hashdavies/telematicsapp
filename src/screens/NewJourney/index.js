import {
  View,
  Text,
  FlatList,
  StyleSheet,
  Button,
  Alert,
  ImageBackground,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {useNavigation} from '@react-navigation/native';
import {generateSimulatedData} from '../../utils/DataGenerator';
import Back4AppUtility from '../../utils/Back4AppUtility';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
// import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MapView, {Marker} from 'react-native-maps';
import JourneyForm from './components/JourneyForm';
import {car} from '../../assets/image';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {SvgXml} from 'react-native-svg';
import {BackArrow} from '../../utils/svg';
import {fontFamilySelector} from '../../utils/fonts';
import {scale} from 'react-native-size-matters';
const defaultLocation = {
  latitude: 6.465422,
  longitude: 3.406448,
  latitudeDelta: 0.0922,
  longitudeDelta: 0.0421,
};

const data = [
  {
    id: '1',
    iconName: 'speedometer',
    iconColor: '#218A10',
    title: 'Speedometer',
    value: '120 km/hr',
    type: 'Speedometer',
  },
  {
    id: '2',
    iconName: 'speed',
    iconColor: '#218A10',
    title: 'RPM Gauge',
    value: '1420',
    type: 'RPMGauge',
  },
  {
    id: '3',
    iconName: 'speedometer-slow',
    iconColor: '#218A10',
    title: 'Fuel Level',
    value: '30 %',
    type: 'FuelLevel',
  },
  {
    id: '4',
    iconName: 'temperature-celsius',
    iconColor: '#218A10',
    title: 'Temperature',
    value: '120 °C',
    type: 'Temperature',
  },
];
export default function NewJourney({navigation}) {
  const [vehicleData, setVehicleData] = useState(generateSimulatedData());
  const [journeyId, setJourneyId] = useState(null);
  const [journeyMetrics, setJourneyMetrics] = useState([]);
  const [isJourneyInProgress, setIsJourneyInProgress] = useState(false);
  const [vehicleDataFormatted, setVehicleDataFormatted] = useState(data);
  const [location, setLocation] = useState(defaultLocation);
  const [loading, setLoading] = useState(false);

  const recordMetric = metric => {
    // console.log(metric, 'Current Metric');
    if (isJourneyInProgress) {
      console.log(metric, 'Current Metric');
      setJourneyMetrics(prevMetrics => [...prevMetrics, metric]);
    }
  };

  useEffect(() => {
    let intervalId; // Declare the interval variable outside the useEffect

    if (isJourneyInProgress) {
      // Start the interval only if the journey is in progress
      intervalId = setInterval(() => {
        const newData = generateSimulatedData();
        setVehicleData(newData);

        // Record the simulated metrics during the journey
        if (isJourneyInProgress) {
          const updatedData = data.map(item => {
            const {type, iconName} = item;
            let value = '';
            switch (type) {
              case 'Speedometer':
                value = `${newData.speed} km/hr`;
                // Check if the speed is abnormal and set the warning color
                if (newData.speed > 150 || newData.speed < 0) {
                  iconColor = '#FF0000'; // Red for abnormal values
                } else {
                  iconColor = '#218A10';
                }
                break;
              case 'RPMGauge':
                value = `${newData.rpm}`;
                // Check if the RPM is abnormal and set the warning color
                if (newData.rpm > 8000 || newData.rpm < 0) {
                  iconColor = '#FF0000'; // Red for abnormal values
                } else {
                  iconColor = '#218A10';
                }
                break;
              case 'FuelLevel':
                value = `${newData.fuelLevel} %`;
                // Check if the fuel level is abnormal and set the warning color
                if (newData.fuelLevel < 10) {
                  iconColor = '#FF0000'; // Red for low fuel
                } else {
                  iconColor = '#218A10';
                }
                break;
              case 'Temperature':
                value = `${newData.engineTemperature} °C`;
                // Check if the temperature is abnormal and set the warning color
                if (
                  newData.engineTemperature > 100 ||
                  newData.engineTemperature < -20
                ) {
                  iconColor = '#FF0000'; // Red for abnormal values
                } else {
                  iconColor = '#218A10';
                }
                break;
              default:
                break;
            }
            let data = {
              ...item,
              value,
              iconColor,
            };
            console.log(data, 'data');
            return data;
          });
          setLocation(prevLocation => ({
            ...prevLocation,
            latitude: newData.latitude,
            longitude: newData.longitude,
          }));
          setVehicleDataFormatted(updatedData);
          recordMetric(newData);
        }
      }, 5000);
    }

    // Clear the interval when the component unmounts or when the journey ends
    return () => clearInterval(intervalId);
  }, [isJourneyInProgress]);

  // Start journey function
  const startJourney = async data => {
    setLoading(true);
    try {
      const {startName, destinationName} = data;
      let TableClass = 'Journey';
      const newData = {
        originName: startName,
        destination: destinationName,
      };
      console.log(newData, 'newData');
      const newJourney = await Back4AppUtility.createRecord(
        TableClass,
        newData,
      );
      console.log('Record created:', newJourney);
      setLoading(false);

      // Set the journeyId to the newly created object's ID
      setJourneyId(newJourney.objectId);
      setIsJourneyInProgress(true);
    } catch (error) {
      console.error('Error:', error);
      setLoading(false);
      showErrorAlert('An error occurred. Please try again.');
    }
  };
  const stopJourney = async () => {
    try {
      if (journeyId) {
        let TableClass = 'Metrics';
        const newData = {
          journeyId: journeyId,
          metrics: journeyMetrics,
        };
        console.log(newData, 'newData');
        const newMetrics = await Back4AppUtility.createRecord(
          TableClass,
          newData,
        );
        console.log(newMetrics, 'newMetrics');
        Alert.alert('Journey Metrics recorded successfully');
        setJourneyId(null);
        setJourneyMetrics([]);
        setIsJourneyInProgress(false);
        navigation.navigate('journeyList');
      }
    } catch (error) {
      console.error('Error:', error);
      setLoading(false);
      showErrorAlert('An error occurred. Please try again.');
    }
  };
  const renderIconComponent = item => {
    const {type, iconName, iconColor} = item;
    switch (type) {
      case 'Speedometer':
        return <SimpleLineIcons name={iconName} size={50} color={iconColor} />;
      case 'RPMGauge':
        return <MaterialIcons name={iconName} size={50} color={iconColor} />;
      case 'FuelLevel':
        return (
          <MaterialCommunityIcons name={iconName} size={50} color={iconColor} />
        );
      case 'Temperature':
        return (
          <MaterialCommunityIcons name={iconName} size={50} color={iconColor} />
        );
      default:
        return null;
    }
  };
  const renderItem = ({item}) => {
    const {iconColor} = item;
    const dynamicStyles = StyleSheet.create({
      title: {
        fontSize: 16,
        marginTop: 8,
        color: iconColor || '#000',
      },
      value: {
        fontSize: 20,
        fontWeight: 'bold',
        color: iconColor || '#000',
      },
    });
    return (
      <View style={styles.card}>
        {renderIconComponent(item)}
        <Text style={dynamicStyles.title}>{item.title}</Text>
        <Text style={dynamicStyles.value}>{item.value}</Text>
      </View>
    );
  };
  return (
    <View style={styles.wrapper}>
      {isJourneyInProgress ? (
        <>
          <FlatList
            data={vehicleDataFormatted}
            ListHeaderComponent={
              <ImageBackground
                source={car}
                imageStyle={{height: '300', resizeMode: 'stretch'}}
                style={{height: 300, width: '100%', marginBottom: 20}}
              />
            }
            numColumns={2}
            keyExtractor={item => item.id}
            ListFooterComponentStyle={{padding: 0}}
            ListFooterComponent={
              <View style={{position: 'relative', marginTop: 10}}>
                <MapView style={styles.map} region={location}>
                  <Marker coordinate={location} />
                </MapView>
              </View>
            }
            renderItem={renderItem}
            contentContainerStyle={styles.container}
          />

          <View style={styles.buttonContainer}>
            <Button
              title={loading ? 'Recording Journey metrics' : 'Stop Journey'}
              onPress={stopJourney}
              style={{height: 200}}
            />
          </View>
        </>
      ) : (
        <View style={styles.appContainer}>
          <JourneyForm onSubmit={startJourney} loading={loading} />
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: 'white',
  },
  container: {
    padding: 16,
    // flex: 1,
  },
  card: {
    flex: 1,
    backgroundColor: '#00A6A60D',
    margin: 8,
    padding: 13,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 16,
    marginTop: 8,
    color: '#000',
  },
  value: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#000',
  },
  map: {
    width: '100%',
    height: 250,
    marginBottom: 12,
  },
  buttonContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    // paddingTop: 30,
    backgroundColor: '#fff', // Adjust the background color as needed
    marginHorizontal: 30,
    marginBottom: 20,
  },
  appContainer: {
    flex: 1,
    justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: 'white',
  },
});

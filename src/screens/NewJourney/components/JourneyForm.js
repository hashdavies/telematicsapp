import React, {useState, useEffect} from 'react';
import {View, Text, TextInput, Button, StyleSheet, Alert} from 'react-native';

function JourneyForm({onSubmit, loading}) {
  const [startName, setStartName] = useState('');
  const [destinationName, setDestinationName] = useState('');

  const submitJourney = () => {
    console.log('Start Name:', startName);
    console.log('Destination Name:', destinationName);
    let data = {startName, destinationName};
    if (startName === '' || destinationName === '') {
      Alert.alert('All field is required');
    } else {
      onSubmit(data);
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Start New Journey</Text>

      <Text style={styles.label}>Start Name:</Text>
      <TextInput
        style={styles.input}
        placeholder="Enter start name"
        value={startName}
        onChangeText={text => setStartName(text)}
      />

      <Text style={styles.label}>Destination Name:</Text>
      <TextInput
        style={styles.input}
        placeholder="Enter destination name"
        value={destinationName}
        onChangeText={text => setDestinationName(text)}
      />

      <Button
        title={loading ? 'Starting Journey' : 'Start Journey'}
        onPress={submitJourney}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 16,
    backgroundColor: '#fff',
  },
  label: {
    fontSize: 16,
    marginBottom: 8,
    color: '#000',
  },
  input: {
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 4,
    padding: 8,
    marginBottom: 16,
    color: '#000',
  },
  header: {
    fontSize: 24,
    marginBottom: 16,
    fontWeight: 'bold',
    color: '#000',
    textAlign: 'center',
  },
});

export default JourneyForm;

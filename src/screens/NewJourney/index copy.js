import {Button, ScrollView, StyleSheet, Text} from 'react-native';
import React, {useEffect, useState} from 'react';
import {useNavigation} from '@react-navigation/native';
import {scale} from 'react-native-size-matters';
import {generateSimulatedData} from '../../utils/DataGenerator';
import VehicleDashboard from './submodules/vehicleDashboard';
import Back4AppUtility from '../../utils/Back4AppUtility';

export default function NewJourney() {
  const {setOptions, toggleDrawer} = useNavigation();
  const [vehicleData, setVehicleData] = useState(generateSimulatedData());
  const [journeyId, setJourneyId] = useState(null);
  const [journeyMetrics, setJourneyMetrics] = useState([]);
  const [isJourneyInProgress, setIsJourneyInProgress] = useState(false);

  const recordMetric = metric => {
    // console.log(metric, 'Current Metric');
    if (isJourneyInProgress) {
      console.log(metric, 'Current Metric');
      // Accumulate metrics during the journey
      setJourneyMetrics(prevMetrics => [...prevMetrics, metric]);
    }
  };

  // useEffect(() => {
  //   // Set up an interval to update data every 5 seconds
  //   const intervalId = setInterval(() => {
  //     const newData = generateSimulatedData();
  //     setVehicleData(newData);
  //     console.log(newData);
  //     // Record the simulated metrics during the journey
  //     if (isJourneyInProgress) {
  //       // recordMetric({type: 'speed', value: newData.speed});
  //       recordMetric(newData);
  //       // recordMetric({type: 'rpm', value: newData.rpm});
  //       // recordMetric({type: 'fuelLevel', value: newData.fuelLevel});
  //       // recordMetric({
  //       //   type: 'engineTemperature',
  //       //   value: newData.engineTemperature,
  //       // });
  //       // recordMetric({type: 'latitude', value: newData.latitude});
  //       // recordMetric({type: 'longitude', value: newData.longitude});
  //     }
  //   }, 5000);

  //   // Clear the interval when the component unmounts
  //   return () => clearInterval(intervalId);
  // }, [isJourneyInProgress]);

  useEffect(() => {
    let intervalId; // Declare the interval variable outside the useEffect

    if (isJourneyInProgress) {
      // Start the interval only if the journey is in progress
      intervalId = setInterval(() => {
        const newData = generateSimulatedData();
        setVehicleData(newData);

        // Record the simulated metrics during the journey
        if (isJourneyInProgress) {
          // recordMetric({type: 'speed', value: newData.speed});
          // recordMetric({type: 'rpm', value: newData.rpm});
          // recordMetric({type: 'fuelLevel', value: newData.fuelLevel});
          // recordMetric({
          //   type: 'engineTemperature',
          //   value: newData.engineTemperature,
          // });
          // recordMetric({type: 'latitude', value: newData.latitude});
          // recordMetric({type: 'longitude', value: newData.longitude});
          recordMetric(newData);
        }
      }, 5000);
    }

    // Clear the interval when the component unmounts or when the journey ends
    return () => clearInterval(intervalId);
  }, [isJourneyInProgress]);

  // Start journey function
  const startJourney = async () => {
    let TableClass = 'Journey';
    const newData = {
      name: 'Testing',
    };
    console.log(newData, 'newData');
    const newJourney = await Back4AppUtility.createRecord(TableClass, newData);
    console.log('Record created:', newJourney);

    // Set the journeyId to the newly created object's ID
    setJourneyId(newJourney.objectId);
    setIsJourneyInProgress(true);
    // }
  };
  const stopJourney = async () => {
    if (journeyId) {
      // Create a new Metrics object and associate it with the Journey
      // const Metrics = 'Metrics';
      // const newMetrics = new Metrics();
      // newMetrics.set('journeyId', journeyId);
      // newMetrics.set('metrics', journeyMetrics);

      // // Save the Metrics object to Back4App
      // await newMetrics.save();
      let TableClass = 'Metrics';
      const newData = {
        journeyId: journeyId,
        metrics: journeyMetrics,
      };
      console.log(newData, 'newData');
      const newMetrics = await Back4AppUtility.createRecord(
        TableClass,
        newData,
      );
      console.log(newMetrics, 'newMetrics');
      // Reset the journeyId, metrics, and journey status
      setJourneyId(null);
      setJourneyMetrics([]);
      setIsJourneyInProgress(false);
    }
  };

  return (
    <ScrollView style={{flex: 1, backgroundColor: '#fff'}}>
      <VehicleDashboard vehicleData={vehicleData} />
      <Text style={styles.titleStyle}>Journey ID: {journeyId}</Text>
      <Text style={styles.titleStyle}>Speed: {vehicleData.speed} km/h</Text>
      <Text style={styles.titleStyle}>RPM: {vehicleData.rpm}</Text>
      <Text style={styles.titleStyle}>
        Fuel Level: {vehicleData.fuelLevel}%
      </Text>
      <Text style={styles.titleStyle}>
        Engine Temperature: {vehicleData.engineTemperature} °C
      </Text>
      <Text style={styles.titleStyle}>Latitude: {vehicleData.latitude}</Text>
      <Text style={styles.titleStyle}>Longitude: {vehicleData.longitude}</Text>
      <Text style={styles.titleStyle}>
        Journey Status: {isJourneyInProgress ? 'In progress' : 'Not Started'}
      </Text>

      <Button title="Start Journey" onPress={startJourney} />
      {/* <View /> */}
      <Button title="Stop Journey" onPress={stopJourney} />
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  Box: {
    backgroundColor: '#d9d9d9',
    marginVertical: 10,
    marginHorizontal: 10,
    paddingHorizontal: 10,
    paddingVertical: 10,
    borderRadius: 10,
  },
  btnText: {
    fontWeight: '600',
    fontSize: 13,
  },
  titleStyle: {
    color: 'black',
    fontWeight: '600',
    fontSize: 13,
    marginBottom: scale(4),
  },
  descStyle: {
    fontSize: 10,
    color: 'black',
    fontWeight: '600',
  },
});

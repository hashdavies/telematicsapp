import {createStackNavigator} from '@react-navigation/stack';
import {StyleSheet, Text, View} from 'react-native';
import JourneyList from '../screens/JourneyList';
import NewJourney from '../screens/NewJourney';
import JourneyDetails from '../screens/JourneyList/JourneyDetails';
import JourneyProgress from '../screens/JourneyProgress';

const Stack = createStackNavigator();

export default function Main() {
  return (
    <Stack.Navigator
      initialRouteName="journeyList"
      screenOptions={{headerShown: false}}>
      <Stack.Screen
        options={{headerShown: false}}
        name="journeyList"
        component={JourneyList}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="newJourney"
        component={NewJourney}
      />
      <Stack.Screen
        options={{headerShown: true}}
        name="journeyDetails"
        component={JourneyDetails}
      />
       <Stack.Screen
        options={{headerShown: true}}
        name="JourneyProgress"
        component={JourneyProgress}
      />
    
    </Stack.Navigator>
  );
}

const styles = StyleSheet.create({});

// DataGenerator.js

export function generateSimulatedData() {
  const speed = Math.floor(Math.random() * 200); // Random speed between 0 and 100 km/h
  const rpm = Math.floor(Math.random() * 9000); // Random RPM between 0 and 8000
  const fuelLevel = Math.floor(Math.random() * 100); // Random fuel level between 0 and 100%
  const engineTemperature = Math.floor(Math.random() * 150); // Random temperature between 0 and 100°C
  const latitude = 6.5244 + Math.random() * 0.1; // Random latitude in Nigeria
  const longitude = 7.5186 + Math.random() * 0.1; // Random longitude in Nigeria

  return {
    speed,
    rpm,
    fuelLevel,
    engineTemperature,
    latitude,
    longitude,
  };
}

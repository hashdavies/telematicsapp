import {MMKV} from 'react-native-mmkv';
export const storage = new MMKV();

export const saveToLocal = (key, Value) => {
  storage.set(key, JSON.stringify(Value));
};
export const fetchFromLocal = key => {
  if (storage.contains(key)) {
    const jsonData = storage.getString(key);
    const dataObject = JSON.parse(jsonData);
    return dataObject;
  } else {
    return null;
  }
};
